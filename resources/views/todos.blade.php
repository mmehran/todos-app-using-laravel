@extends('layout')

@section('content')
    <div class="container" >
        <div class="row justify-content-md-center">    
        <div class="col-md-8 col-md-offset-5">
             <form action="/create/todo" method="POST">
                {{csrf_field()}}
                    <input type="text" class="form-control input-lg" name="todo" placeholder="Create new todo">
             </form>
        
    <hr>
    @foreach($todos as $todo)
    
        {{ $todo->todo }}
        <br> 
        <a href="{{route('todo.delete',['id' => $todo->id]) }}" class="btn btn-danger">X</a>
        <a href="{{route('todo.update',['id' => $todo->id]) }}" class="btn btn-xs btn-info">Update</a>
        @if(!$todo->completed)
        <a href="{{route('todo.completed',['id' => $todo->id])}}" class="btn btn-xs btn-success">Mark as Completed</a>
        @else
        <spam class="text-success mark">Completed</spam>
        @endif
        <hr>
    @endforeach
</div>
</div>
</div>

@stop